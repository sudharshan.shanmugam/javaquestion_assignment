package Car_Package;

public class MainClass_Car {
	
	 public static void main(String[] args) {
	        Car car = new SantroCar();
	        car.drive();
	        car.stop();
	        ((SantroCar) car).remotestart();

	        BasicCar basicCar = (BasicCar) car;
	        basicCar.gearchange();
	        basicCar.music();
	    }
	}


