package SmartTV_Package;

public class SmartTelephone extends Telephone{
	
	  
	    @Override
	    void dial() {
	        System.out.println("Dialing from Smart Telephone");
	    }

	    @Override
	    void lift() {
	        System.out.println("Lifting from Smart Telephone");
	    }

	    @Override
	    void disconnected() {
	        System.out.println("Disconnected from Smart Telephone");
	    }
	}
	


