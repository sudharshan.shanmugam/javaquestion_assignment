package Single_Program;

public class Orange {
	

	public static void main(String[] args) {
		
	    String input = "This is orange juice";
	    
	    if (input.toLowerCase().contains("orange")) {
	    	
	      System.out.println("The word 'orange' is present in the input string.");
	    } 
	    else {
	      System.out.println("The word 'orange' is NOT present in the input string.");
	    }
	  
	
	}
	

}
