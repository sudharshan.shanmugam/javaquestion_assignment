package SmartTV_Package;

public class TV  implements SmartTVremote{
	
	    @Override
	    public void volumeUp() {
	        System.out.println("Volume Up");
	    }

	    @Override
	    public void volumeDown() {
	        System.out.println("Volume Down");
	    }

	    @Override
	    public void channelUp() {
	        System.out.println("Channel Up");
	    }

	    @Override
	    public void channelDown() {
	        System.out.println("Channel Down");
	    }

}
