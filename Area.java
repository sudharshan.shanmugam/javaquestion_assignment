package Shape_Package;

public class Area extends Shape{

	 
		  @Override
		  void RectangleArea(double length, double breadth) {
		    System.out.println("Rectangle area: " + (length * breadth));
		  }
		  
		  @Override
		  void SquareArea(double side) {
		    System.out.println("Square area: " + (side * side));
		  }
		  
		  @Override
		  void CircleArea(double radius) {
		    System.out.println("Circle area: " + (Math.PI * radius * radius));
		  }
		}
	

