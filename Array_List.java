package Single_Program;

import java.util.List;

public class Array_List {
	
	public static void main(String[] args) {
        
		List<String> li = new java.util.ArrayList<>();

		li.add("Coke");
		li.add("Pepsi");
		li.add("Fanta");
		li.add("Mazaa");
		li.add("Sprit");

		System.out.println(li);

		int s=li.size();
		System.out.println("Size of the list:"+s);
		String s1 =li.remove(1);
		System.out.println("2nd item removed:"+s1);
		System.out.println(li);

		boolean s3 = li.contains("Coke");
		System.out.println("Coke is present:"+s3);

		    }

}
