package Car_Package;

public class SantroCar extends Car implements BasicCar{
	
	 @Override
	 public void gearchange() {
	        System.out.println("Changing gears in a Santro");
	    }

	    @Override
	    public void music() {
	        System.out.println("Playing music in a Santro");
	    }

	    public void remotestart() {
	        System.out.println("Remote starting a Santro");
	    }
	}

	interface BasicCar {
	    void gearchange();
	    void music();
	}

	
